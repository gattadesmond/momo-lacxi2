// First we get the viewport height and we multiple it by 1% to get a value for a vh unit
let vh = window.innerHeight * 0.01;
// Then we set the value in the --vh custom property to the root of the document
document.documentElement.style.setProperty("--vh", `${vh}px`);

// We listen to the resize event
// window.addEventListener("resize", () => {
//   // We execute the same script as before
//   let vh = window.innerHeight * 0.01;
//   document.documentElement.style.setProperty("--vh", `${vh}px`);
// });

imagesLoaded(document.body, () => document.body.classList.add("loaded"));

function navbarFixed() {
  $(window).scroll(function() {
    var scroll = $(window).scrollTop();
    if (scroll) {
      $(".navbar-lacxi").addClass("is-fixed");
    } else {
      $(".navbar-lacxi").removeClass("is-fixed");
    }
  });
  $(window).scroll();
}
navbarFixed();

$(".navbar-lacxi").on("click", function() {
  $(".navbar-collapse").collapse("hide");
});

var swiper = new Swiper(".swiper-banner", {
  slidesPerView: "2",
  spaceBetween: 20,
  autoplay: {
    delay: 4000
  },
  pagination: {
    el: ".swiper-pagination"
  },
  breakpoints: {
    640: {
      slidesPerView: 1,
      spaceBetween: 20
    },
    768: {
      slidesPerView: 2,
      spaceBetween: 20
    },
    1024: {
      slidesPerView: 2,
      spaceBetween: 20
    }
  }
});

$(".a-nav-toggle").on("click", function() {
  if ($("html").hasClass("body-menu-opened")) {
    $("html")
      .removeClass("body-menu-opened")
      .addClass("body-menu-close");
  } else {
    $("html")
      .addClass("body-menu-opened")
      .removeClass("body-menu-close");
  }
});

var htuSwiper = new Swiper(".htu-slider", {
  loop: false,
  // Disable preloading of all images
  preloadImages: false,
  // Enable lazy loading
  lazy: {
    loadPrevNext: true
  },
  slidesPerView: 1,

  effect: "fade",
  fadeEffect: {
    crossFade: true
  },
  navigation: {
    nextEl: ".swiper-button-next",
    prevEl: ".swiper-button-prev"
  }
});

$(".htu-modal").on("shown.bs.modal", function () {
  var mySwiper = this.querySelector(".swiper-container").swiper;
  mySwiper.slideTo(0);
  mySwiper.update();

  // $(this).find(".htu-slider").swiper().update();
});
var headerHideAuto = function() {
  var mainHeader = $(".navbar-lacxi");
  var mainHeader2 = $(".navbar-lacxi2");
  var scrolling = false,
    previousTop = 0,
    currentTop = 0,
    scrollDelta = 10,
    scrollOffset = 150;
    
  function autoHideHeader() {
    var currentTop = $("html").scrollTop();
    if (previousTop - currentTop > scrollDelta) {
      mainHeader.removeClass("is-hidden");
      mainHeader2.removeClass("is-hidden");
    } else if (
      currentTop - previousTop > scrollDelta &&
      currentTop > scrollOffset
    ) {
      mainHeader.addClass("is-hidden");
      mainHeader2.addClass("is-hidden");
    }

    previousTop = currentTop;
    scrolling = false;
  }

  $(window).on("scroll", function() {
    if (!scrolling) {
      scrolling = true;
      !window.requestAnimationFrame
        ? setTimeout(autoHideHeader, 250)
        : requestAnimationFrame(autoHideHeader);
    }
  });
};
headerHideAuto();


// Caching some stuff..
const body = document.body;
const docEl = document.documentElement;

$(".manual-device").each(function(index, element) {
  var $this = $(this);
  var numSlider = $this.attr("childId");
  var device = null;
  var process = null;

  if (numSlider != undefined) {
    device = $this.find(".manual-device-swiper#hd-sub-swp-" + numSlider);
    process = $this
      .parent()
      .parent()
      .find(".manual-process#hd-sub-ctn-" + numSlider);
  } else {
    numSlider = index;
    device = $this.find(".manual-device-swiper");

    process = $this
      .parent()
      .parent()
      .find(".manual-process ");
  }

  var swiperArray = [];

  swiperArray[numSlider] = new Swiper(device, {
    observer: true,
    observeParents: true,
    on: {
      slideChangeTransitionEnd: function slideChangeTransitionEnd() {
        var num = this.activeIndex;
        process.children(".process_item").removeClass("active");
        process
          .children(".process_item")
          .eq(num)
          .addClass("active");
      }
    }
  });

  process.children(".process_item").each(function(index, element) {
    var num = index;
    $(element).on("click", function(e) {
      if ($(this).hasClass("active")) return;
      e.preventDefault();
      $(this)
        .addClass("active")
        .siblings()
        .removeClass("active");
      swiperArray[numSlider].slideTo(index);
    });
  });
});

$(".process__body-content").each(function(index, element) {
  var $this = $(this);
  var height = $this[0].scrollHeight;
  var getHeight = height == "0" ? "auto" : height + "px";
  $this.css("--max-height", getHeight);
});



$(document).ready(function(){
  $('[data-toggle="popover"]').popover();

  var swiper = new Swiper('.quote-swiper', {
    speed: 1500,
    pagination: {
      el: '.swiper-pagination',
      clickable: true
    },
    loop: true,
    spaceBetween: 30,
    autoplay: {
      delay: 9000,
      disableOnInteraction: false,
    },
  });
})